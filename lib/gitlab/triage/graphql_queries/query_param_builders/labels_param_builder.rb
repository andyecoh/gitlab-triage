require_relative '../../utils'
require_relative 'base_param_builder'

module Gitlab
  module Triage
    module GraphqlQueries
      module QueryParamBuilders
        class LabelsParamBuilder < BaseParamBuilder
          def initialize(param_name, labels, negated: false)
            label_param_content = labels.map { |label| Utils.graphql_quote(label) }.join(', ').then { |content| "[#{content}]" }

            super(param_name, label_param_content, with_quotes: false, negated: negated)
          end
        end
      end
    end
  end
end
